const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  devtool: 'cheap-module-eval-source-map',

  devServer: {
    contentBase: './dist',
    hot: true,
    noInfo: true
  },

	entry: './client/index.js',

	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: '[name].[hash].js',
		publicPath: 'http://0.0.0.0:8080'
	},

	module: {
		rules: [
			{
				test: /\.css$/,
				exclude: '/node_modules/',
				use: 'css-loader'
			},
			{
				test: /\.js|jsx$/,
				exclude: '/node_modules/',
				use: {
	        loader: 'babel-loader',
	        options: {
	          presets: ['@babel/preset-react']
	        }
	      }
			},
			{
        test: /\.(jpe?g|gif|png|svg)$/i,
        loader: 'url-loader?limit=10000'
			}
		]
	},

	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new HtmlWebpackPlugin()
	],

	mode: 'development'
};